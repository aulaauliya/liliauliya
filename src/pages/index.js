import Detail from "./Detail";
import Home from "./Home";
import Login from "./Login";
import Splash from "./Splash";

export {Login, Splash, Home, Detail};

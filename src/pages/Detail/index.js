import React from 'react';
import { ScrollView, StyleSheet, Text, View,  } from 'react-native';

const Detail = (data) => {
    const ayat = data.route.params.ayat;
    console.log(ayat)
    return (
        <View>

            <ScrollView>
                {ayat.map(item => {
                return (
                
                    <View style={styles.texbox}>
                            <Text style={{fontSize: 20}}>{item.text}</Text>
                            <Text style={{fontSize: 10}}>{item.translation_id}</Text>
                    </View>
                )

                })}
            <Text>Detail</Text>
            
            </ScrollView>
        </View>
    )
}

export default Detail

const styles = StyleSheet.create({

     btn: {
            backgroundColor: 'blue',
            padding: 50,
     },
    
})

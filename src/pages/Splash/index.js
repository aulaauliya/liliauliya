import React, {useEffect} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';

const Splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Home');
        },2000);
    });
    return ( 
        <View style={styles.container}>
            <View style={styles.wraper}>
                <Image source={require('../Splash/logo2.png')} style={{width:100, height:100}}/>
                <Text style={{alignItems:'center', WebkitJustifyContent:'center', backgroundColor:'green', textAlign:'center'}}>MARI MENGAJI!</Text>
                <Text style={{alignItems:'center', WebkitJustifyContent:'center', backgroundColor:'green', textAlign:'center',marginTop:350,}}>Semoga bermanfaat untuk kita semua</Text>
                <Text style={{alignItems:'center', WebkitJustifyContent:'center', backgroundColor:'green', textAlign:'center',}}>Segala puji bagi Allah</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: 'green',
        justifyContent:'center',
        alignItems:'center',
     },
     wraper: {
         flex: 1,
         justifyContent:'center',
        alignItems:'center',

     },
     
});

export default Splash;
 
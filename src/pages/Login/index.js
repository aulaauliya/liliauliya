import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

const Login = () => {
  return (
    <View style={styles.container}>
      <View style={styles.boxImg}>
        <Image source={require('../Splash/logo1.jpg')} style={styles.img} />

      </View>
      <View style={styles.kotak}>
        <Text style={styles.text}>10</Text>
      </View>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5,    
  },
  boxImg: {
    backgroundColor: 'yellow',
    marginTop: 30,
    width: 320,
    height: 320,
    padding: 10,
  },
  img: {
    width:300,
    height:300,
  },
  kotak: {
    height: 100,
    width: 100,
    borderWidth: 1,
    borderColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'green',
    borderRadius: 10,
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
  }
});
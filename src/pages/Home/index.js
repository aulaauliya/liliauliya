import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, ScrollView } from 'react-native'
import data from '../../data/quran-complete.json'

console.log(data)

export default class Home extends Component {
    render() {
        return (
        
            <ScrollView>
                {data.map(item => {
                    return (
                        <TouchableOpacity key={item.number_of_surah} onPress={() => this.props.navigation.navigate('Detil', {ayat: item.verses})} style={styles.btn}>
                            <View style={styles.texbox}>
                                <Text style={styles.text}>{item.number_of_surah}</Text>
                                <Text style={styles.text}>{item.name}</Text>
                                <Text style={styles.text}>{item.name_translations.ar}</Text>
                            </View>
                        </TouchableOpacity>
                        
                    )
                })}
                <Text> home </Text>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    
    btn: {
        backgroundColor: 'pink',
        marginBottom: 2,
        padding: 10,
    },
    texbox: {
        flexDirection: 'row',
        flex: 1,
    },
    text: {
        flex: 1,
    }
})

import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import {Splash, Login, Home, Detail} from '../pages';

const Stack = createStackNavigator();

const Route = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="Splash"
                component={Splash}
                options={{headerShown: false}}
            />
            <Stack.Screen
                name="Login"
                component={Login}
                options={{headerShown: false}}
            />
            <Stack.Screen
                name="Detil"
                component={Detail}
                options={{headerShown: true}}
            />
            <Stack.Screen
                name="Home"
                component={Home}
                options={{headerShown: false}}
            />
        </Stack.Navigator>
    );
};

export default Route;